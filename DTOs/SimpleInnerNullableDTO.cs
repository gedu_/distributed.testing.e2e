using Amqp.Serialization;

namespace Distributed.Testing.E2E
{
    [AmqpContract(Encoding = EncodingType.List)]
    public class SimpleInnerNullableDTO
    {
        [AmqpMember]
        public int? IntValue { get; set; }

        [AmqpMember]
        public double? DoubleValue { get; set; }
    }
}