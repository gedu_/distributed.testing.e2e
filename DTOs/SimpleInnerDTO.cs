using Amqp.Serialization;

namespace Distributed.Testing.E2E
{
    [AmqpContract(Encoding = EncodingType.List)]
    public class SimpleInnerDTO
    {
        [AmqpMember]
        public int IntValue { get; set; }

        [AmqpMember]
        public string StringValue { get; set; }
    }
}