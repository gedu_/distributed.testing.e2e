using Amqp.Serialization;

namespace Distributed.Testing.E2E
{
    [AmqpContract(Encoding = EncodingType.List)]
    public class SimpleDTO
    {
        [AmqpMember]
        public byte ByteValue { get; set; }

        [AmqpMember]
        public sbyte SbyteValue { get; set; }

        [AmqpMember]
        public int IntValue { get; set; }

        [AmqpMember]
        public uint UintValue { get; set; }

        [AmqpMember]
        public short ShortValue { get; set; }

        [AmqpMember]
        public ushort UshortValue { get; set; }

        [AmqpMember]
        public long LongValue { get; set; }

        [AmqpMember]
        public ulong UlongValue { get; set; }

        [AmqpMember]
        public float FloatValue { get; set; }

        [AmqpMember]
        public double DoubleValue { get; set; }

        [AmqpMember]
        public char CharValue { get; set; }

        [AmqpMember]
        public bool BoolValue { get; set; }

        [AmqpMember]
        public string StringValue { get; set; }

        [AmqpMember]
        public SimpleInnerDTO InnerDTO { get; set; }
    }
}
