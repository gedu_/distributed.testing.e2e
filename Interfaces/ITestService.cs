using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Distributed.Common.Attributes;

namespace Distributed.Testing.E2E.Interfaces
{
    [ServiceContract]
    public interface ITestService {
        [OperationContract()]
        string Greet();

        [OperationContract(Name = "GreetWithName")]
        string Greet(string name);

        [OperationContract()]
        [ServiceTimeout(1)]
        bool TestTimeout();
        
        [OperationContract]
        decimal Add(decimal first, decimal second, decimal? third);

        [OperationContract(IsOneWay = true)]
        Task TestOneWay();

        //Testing data types

        //simple types

        [OperationContract]
        byte TestByte();

        [OperationContract]
        sbyte TestSByte();

        [OperationContract]
        int TestInt();

        [OperationContract]
        uint TestUInt();

        [OperationContract]
        short TestShort();

        [OperationContract]
        ushort TestUShort();

        [OperationContract]
        long TestLong();

        [OperationContract]
        ulong TestULong();

        [OperationContract]
        float TestFloat();

        [OperationContract]
        double TestDouble();

        [OperationContract]
        char TestChar();

        [OperationContract]
        bool TestBool();

        [OperationContract]
        string TestString();

        [OperationContract]
        decimal TestDecimal();

        //Conventional nullable types

        [OperationContract]
        byte? TestNullableByte(bool withValue);

        [OperationContract]
        sbyte? TestNullableSByte(bool withValue);

        [OperationContract]
        int? TestNullableInt(bool withValue);

        [OperationContract]
        uint? TestNullableUInt(bool withValue);

        [OperationContract]
        short? TestNullableShort(bool withValue);

        [OperationContract]
        ushort? TestNullableUShort(bool withValue);

        [OperationContract]
        long? TestNullableLong(bool withValue);

        [OperationContract]
        ulong? TestNullableULong(bool withValue);

        [OperationContract]
        float? TestNullableFloat(bool withValue);

        [OperationContract]
        double? TestNullableDouble(bool withValue);

        [OperationContract]
        char? TestNullableChar(bool withValue);

        [OperationContract]
        bool? TestNullableBool(bool withValue);

        [OperationContract]
        decimal? TestNullableDecimal(bool withValue);

        //Tasks with simple types

        [OperationContract]
        Task<byte> TestByteAsync();

        [OperationContract]
        Task<sbyte> TestSByteAsync();

        [OperationContract]
        Task<int> TestIntAsync();

        [OperationContract]
        Task<uint> TestUIntAsync();

        [OperationContract]
        Task<short> TestShortAsync();

        [OperationContract]
        Task<ushort> TestUShortAsync();

        [OperationContract]
        Task<long> TestLongAsync();

        [OperationContract]
        Task<ulong> TestULongAsync();

        [OperationContract]
        Task<float> TestFloatAsync();

        [OperationContract]
        Task<double> TestDoubleAsync();

        [OperationContract]
        Task<char> TestCharAsync();

        [OperationContract]
        Task<bool> TestBoolAsync();

        [OperationContract]
        Task<string> TestStringAsync();

        [OperationContract]
        Task<decimal> TestDecimalAsync();


        //Tasks with conventional nullable types

        [OperationContract]
        Task<byte?> TestNullableByteAsync(bool withValue);

        [OperationContract]
        Task<sbyte?> TestNullableSByteAsync(bool withValue);

        [OperationContract]
        Task<int?> TestNullableIntAsync(bool withValue);

        [OperationContract]
        Task<uint?> TestNullableUIntAsync(bool withValue);

        [OperationContract]
        Task<short?> TestNullableShortAsync(bool withValue);

        [OperationContract]
        Task<ushort?> TestNullableUShortAsync(bool withValue);

        [OperationContract]
        Task<long?> TestNullableLongAsync(bool withValue);

        [OperationContract]
        Task<ulong?> TestNullableULongAsync(bool withValue);

        [OperationContract]
        Task<float?> TestNullableFloatAsync(bool withValue);

        [OperationContract]
        Task<double?> TestNullableDoubleAsync(bool withValue);

        [OperationContract]
        Task<char?> TestNullableCharAsync(bool withValue);

        [OperationContract]
        Task<bool?> TestNullableBoolAsync(bool withValue);

        [OperationContract]
        Task<decimal?> TestNullableDecimalAsync(bool withValue);

        //Complex types
        [OperationContract]
        SimpleDTO TestSimpleDTO();

        //Complex nullable types
        [OperationContract]
        SimpleNullableDTO TestSimpleNullableDTO(bool withValue);

        //Task complex types
        [OperationContract]
        Task<SimpleDTO> TestSimpleDTOAsync();

        //Task complex nullable types
        [OperationContract]
        Task<SimpleNullableDTO> TestSimpleNullableDTOAsync(bool withValue);

        //
        [OperationContract]
        int TestListSum(List<int> numbers);


    }
}
