# Distributed.Testing.E2E
## Version 1.0

Tests all available features on a live environment.

### Usage

Change ampq connection string in settings.json if necessary

#### Available modes:

* sync - Tests interface calling with various return types synchroniously 

* async - Tests interface calling with various return types asynchroniously 

* syncpubsub - Tests PubSub interface synchroniously

* asyncpubsub Tests PubSub interface asynchroniously

* syncdto Tests DTO with various return types synchroniously

* asyncdto Tests DTO with various return types asynchroniously

#### Command:

dotnet run -m <mode>

### Known issues

Async tests currently fail on oneway test

Tests hang for 1 minute occasionaly
