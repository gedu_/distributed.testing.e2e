using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class CommonTests
    {
        public static int Execute(ITestService testService, ILogger logger)
        {
            var status = 0;

            //Test Overloading
            var expectedDefaultGreet = string.Format(Constants.Greet, Constants.DefaultUser);
            var defaultGreet = testService.Greet();

            if (defaultGreet != expectedDefaultGreet)
            {
                logger.LogCritical($"Incorrect value returned from overloadable method without any parameters. {defaultGreet} != {expectedDefaultGreet}");
                status++;
            }

            var greetName = "Test";
            var expectedOverloadedGreet = string.Format(Constants.Greet, greetName);
            var overloadedGreet = testService.Greet(greetName);

            if (overloadedGreet != expectedOverloadedGreet)
            {
                logger.LogCritical($"Incorrect value returned from overloaded method without parameter {greetName}. {overloadedGreet} != {expectedOverloadedGreet}");
                status++;
            }

            ////////////////////////////

            //Test timeout
            try 
            {
                var timeoutedRequest = testService.TestTimeout();

                logger.LogCritical($"TimeoutException was not thrown");
                status++;
            }
            catch (InvalidCastException)
            {

            }
            catch (Exception e)
            {
                logger.LogCritical($"Expected InvalidCastException, got {e.GetType()}");
                status++;
            }

            ////////////////////////////

            //Testing one way operation
            
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            testService.TestOneWay().Wait();

            stopwatch.Stop();

            if (stopwatch.Elapsed.Milliseconds >= 5000)
            {
                logger.LogCritical("Oneway operation was not executed separately");
                status++;
            }

            //Test argument calling
            var firstMember = 11M;
            var secondMember = 123.1M;
            decimal? thirdMember = null;

            var expectedResult = firstMember + secondMember;

            var addition = testService.Add(firstMember, secondMember, thirdMember);

            if (addition != expectedResult)
            {
                logger.LogCritical($"Incorrect addition result. {addition} != {expectedResult}");
                status++;
            }

            thirdMember = 44.1M;

            expectedResult = firstMember + secondMember + thirdMember.Value;

            addition = testService.Add(firstMember, secondMember, thirdMember);

            if (addition != expectedResult)
            {
                logger.LogCritical($"Incorrect addition result. {addition} != {expectedResult}");
                status++;
            }

            //Test list argument
            var numbers = new List<int>()
            {
                1,
                2,
                3,
                4,
                5
            };

            var sum = testService.TestListSum(numbers);

            var expectedSumResult = numbers.Sum();

            if (sum != expectedSumResult)
            {
                logger.LogCritical($"Incorrect list sum result. {sum} != {expectedSumResult}");
                status++;
            }

            return status;
        }
    }
}