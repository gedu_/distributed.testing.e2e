using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class SyncPubSubTests
    {
        public static int Execute(List<IServiceProvider> ClientProviders, ILogger logger)
        {
            var status = 0;

            logger.LogInformation("[PubSub] Starting synchronous tests ...");

            status += PubSubSync.Test(ClientProviders, logger); 

            logger.LogInformation("[PubSub] Finished synchronous tests ...");

            return status;
        }
    }
}