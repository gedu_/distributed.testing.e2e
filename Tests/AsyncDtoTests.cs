using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Distributed.Common.Interfaces;
using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class AsyncDtoTests
    {
        public static int Execute(IServiceLoop serviceLoop, List<IServiceProvider> clientProviders, ILogger logger)
        {
            logger.LogInformation("[DTO] Starting asynchronous tests ...");

            var status = 0;

            var cancellationTokenSource = new CancellationTokenSource();

            Task.Factory.StartNew(async () =>  await serviceLoop.RunAsync(), cancellationTokenSource.Token);

            var provider = clientProviders[0];
            var proxy = (ICallProxy)provider.GetService(typeof(ICallProxy));

            var testService = (ITestService)proxy.CreateProxy(typeof(ITestService));

            ////////////////////////////

            //Test Simple DTOs

            logger.LogInformation("Starting Simple DTOs testing ...");

            status += SimpleDTOs.Test(testService, logger);

            logger.LogInformation("Finished Simple DTOs testing ...");

            //Test Simple nullable DTOs

            logger.LogInformation("Starting Simple Nullable DTOs testing ...");

            status += SimpleNullableDTOs.Test(testService, logger);

            logger.LogInformation("Finished Simple Nullable DTOs testing ...");

            //Test tasks with Simple DTOs

            logger.LogInformation("Starting Tasks with Simple DTOs testing ...");

            status += TaskWithSimpleDTOs.Test(testService, logger).Result;

            logger.LogInformation("Finished Tasks with Simple DTOs testing ...");


            //Test tasks with Simple nullable DTOs

            logger.LogInformation("Starting Tasks with Simple Nullable DTOs testing ...");

            status += TaskWithSimpleNullableDTOs.Test(testService, logger).Result;

            logger.LogInformation("Finished Tasks with Simple Nullable DTOs testing ...");

            ////////////////////////////


            //kill / turn off thread
            serviceLoop.Stop();

            cancellationTokenSource.Cancel();

            cancellationTokenSource.Dispose();

            logger.LogInformation("[DTO] Finished asynchronous tests ...");

            return status;
        }
    }
}