using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class AsyncPubSubTests
    {
        public static int Execute(List<IServiceProvider> ClientProviders, ILogger logger)
        {
            var status = 0;

            logger.LogInformation("[PubSub] Starting asynchronous tests ...");

            status += PubSubAsync.Test(ClientProviders, logger);

            logger.LogInformation("[PubSub] Finished asynchronous tests ...");

            return status;
        }
    }
}