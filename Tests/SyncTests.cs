using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Distributed.Common.Interfaces;
using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class SyncTests
    {
        public static int Execute(IServiceLoop serviceLoop, List<IServiceProvider> clientProviders, ILogger logger)
        {
            logger.LogInformation("[Messenger] Starting synchronous tests ...");

            var status = 0;

            var cancellationTokenSource = new CancellationTokenSource();

            Task.Factory.StartNew(() =>  {
                serviceLoop.Run();
            }, cancellationTokenSource.Token);

            var provider = clientProviders[0];
            var proxy = (ICallProxy)provider.GetService(typeof(ICallProxy));

            var testService = (ITestService)proxy.CreateProxy(typeof(ITestService));

            ////////////////////////////

            //Test Simple framework features

            status += CommonTests.Execute(testService, logger);

            ////////////////////////////

            //Test Simple types

            logger.LogInformation("Starting Simple types testing ...");

            status += SimpleTypes.Test(testService, logger);

            logger.LogInformation("Finished Simple types testing ...");

            //Test simple nullable types

            logger.LogInformation("Starting Simple Nullable types testing ...");

            status += SimpleNullableTypes.Test(testService, logger);

            logger.LogInformation("Finished Simple Nullable types testing ...");

            //Test tasks with simple types

            logger.LogInformation("Starting Tasks with Simple types testing ...");

            status += TasksWithSimpleTypes.Test(testService, logger).Result;

            logger.LogInformation("Finished Tasks with Simple types testing ...");


            //Test tasks with simple nullable types

            logger.LogInformation("Starting Tasks with Simple Nullable types testing ...");

            status += TasksWithSimpleNullableTypes.Test(testService, logger).Result;

            logger.LogInformation("Finished Tasks with Simple Nullable types testing ...");

            ////////////////////////////


            //kill / turn off thread
            serviceLoop.Stop();

            cancellationTokenSource.Cancel();

            cancellationTokenSource.Dispose();

            logger.LogInformation("[Messenger] Finished synchronous tests ...");

            return status;
        }
    }
}