using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Distributed.Testing.E2E.Interfaces;

namespace Distributed.Testing.E2E
{
    public class ExampleService : ITestService
    {

        public decimal Add(decimal first, decimal second, decimal? third)
        {
            return third.HasValue ? first + second + third.Value : first + second;
        }

        public async Task TestOneWay()
        {
            await Task.Delay(5000);
        }

        public string Greet()
        {
            return string.Format(Constants.Greet, Constants.DefaultUser);
        }

        public string Greet(string name)
        {
            return string.Format(Constants.Greet, name);
        }

        public bool TestTimeout()
        {
            Thread.Sleep(1000);
            return true;
        }

        public byte TestByte()
        {
            return (byte)Constants.DefaultIntReturn;
        }

        public sbyte TestSByte()
        {
            return (sbyte)Constants.DefaultIntReturn;
        }

        public int TestInt()
        {
            return Constants.DefaultIntReturn;
        }

        public uint TestUInt()
        {
            return (uint)Constants.DefaultIntReturn;
        }

        public short TestShort()
        {
            return (short)Constants.DefaultIntReturn;
        }

        public ushort TestUShort()
        {
            return (ushort)Constants.DefaultIntReturn;
        }

        public long TestLong()
        {
            return (long)Constants.DefaultIntReturn;
        }

        public ulong TestULong()
        {
            return (ulong)Constants.DefaultIntReturn;
        }

        public float TestFloat()
        {
            return (float)Constants.DefaultIntReturn;
        }

        public double TestDouble()
        {
            return (double)Constants.DefaultIntReturn;
        }

        public char TestChar()
        {
            return Constants.DefaultCharReturn;
        }

        public bool TestBool()
        {
            return true;
        }

        public string TestString()
        {
            return Constants.DefaultStringReturn;
        }

        public decimal TestDecimal()
        {
            return Constants.DefaultDecimalReturn;
        }

        //Simple types nullables

        public byte? TestNullableByte(bool withValue)
        {
            return withValue ? (byte?)Constants.DefaultIntReturn : null;
        }

        public sbyte? TestNullableSByte(bool withValue)
        {
            return withValue ? (sbyte?)Constants.DefaultIntReturn : null;
        }

        public int? TestNullableInt(bool withValue)
        {
            return withValue ? (int?)Constants.DefaultIntReturn : null;
        }

        public uint? TestNullableUInt(bool withValue)
        {
            return withValue ? (uint?)Constants.DefaultIntReturn : null;
        }

        public short? TestNullableShort(bool withValue)
        {
            return withValue ? (short?)Constants.DefaultIntReturn : null;
        }

        public ushort? TestNullableUShort(bool withValue)
        {
            return withValue ? (ushort?)Constants.DefaultIntReturn : null;
        }

        public long? TestNullableLong(bool withValue)
        {
            return withValue ? (long?)Constants.DefaultIntReturn : null;
        }

        public ulong? TestNullableULong(bool withValue)
        {
            return withValue ? (ulong?)Constants.DefaultIntReturn : null;
        }

        public float? TestNullableFloat(bool withValue)
        {
            return withValue ? (float?)Constants.DefaultIntReturn : null;
        }

        public double? TestNullableDouble(bool withValue)
        {
            return withValue ? (double?)Constants.DefaultIntReturn : null;
        }

        public char? TestNullableChar(bool withValue)
        {
            return withValue ? (char?)Constants.DefaultCharReturn : null;
        }

        public bool? TestNullableBool(bool withValue)
        {
            return withValue ? (bool?)true : null;
        }

        public decimal? TestNullableDecimal(bool withValue)
        {
            return withValue ? (decimal?)Constants.DefaultDecimalReturn : null;
        }

        //tasks simple type

        public Task<byte> TestByteAsync()
        {
            return Task.FromResult((byte)Constants.DefaultIntReturn);
        }

        public Task<sbyte> TestSByteAsync()
        {
            return Task.FromResult((sbyte)Constants.DefaultIntReturn);
        }

        public Task<int> TestIntAsync()
        {
            return Task.FromResult(Constants.DefaultIntReturn);
        }

        public Task<uint> TestUIntAsync()
        {
            return Task.FromResult((uint)Constants.DefaultIntReturn);
        }

        public Task<short> TestShortAsync()
        {
            return Task.FromResult((short)Constants.DefaultIntReturn);
        }

        public Task<ushort> TestUShortAsync()
        {
            return Task.FromResult((ushort)Constants.DefaultIntReturn);
        }

        public Task<long> TestLongAsync()
        {
            return Task.FromResult((long)Constants.DefaultIntReturn);
        }

        public Task<ulong> TestULongAsync()
        {
            return Task.FromResult((ulong)Constants.DefaultIntReturn);
        }

        public Task<float> TestFloatAsync()
        {
            return Task.FromResult((float)Constants.DefaultIntReturn);
        }

        public Task<double> TestDoubleAsync()
        {
            return Task.FromResult((double)Constants.DefaultIntReturn);
        }

        public Task<char> TestCharAsync()
        {
            return Task.FromResult(Constants.DefaultCharReturn);
        }

        public Task<bool> TestBoolAsync()
        {
            return Task.FromResult(true);
        }

        public Task<string> TestStringAsync()
        {
            return Task.FromResult(Constants.DefaultStringReturn);
        }

        public Task<decimal> TestDecimalAsync()
        {
            return Task.FromResult((decimal)Constants.DefaultDecimalReturn);
        }

        //Tasks nullable simple type

        public Task<byte?> TestNullableByteAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (byte?)Constants.DefaultIntReturn : null);
        }

        public Task<sbyte?> TestNullableSByteAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (sbyte?)Constants.DefaultIntReturn : null);
        }

        public Task<int?> TestNullableIntAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (int?)Constants.DefaultIntReturn : null);
        }

        public Task<uint?> TestNullableUIntAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (uint?)Constants.DefaultIntReturn : null);
        }

        public Task<short?> TestNullableShortAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (short?)Constants.DefaultIntReturn : null);
        }

        public Task<ushort?> TestNullableUShortAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (ushort?)Constants.DefaultIntReturn : null);
        }

        public Task<long?> TestNullableLongAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (long?)Constants.DefaultIntReturn : null);
        }

        public Task<ulong?> TestNullableULongAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (ulong?)Constants.DefaultIntReturn : null);
        }

        public Task<float?> TestNullableFloatAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (float?)Constants.DefaultIntReturn : null);
        }

        public Task<double?> TestNullableDoubleAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (double?)Constants.DefaultIntReturn : null);
        }

        public Task<char?> TestNullableCharAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (char?)Constants.DefaultCharReturn : null);
        }

        public Task<bool?> TestNullableBoolAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (bool?)true : null);
        }

        public Task<decimal?> TestNullableDecimalAsync(bool withValue)
        {
            return Task.FromResult(withValue ? (decimal?)Constants.DefaultDecimalReturn : null);
        }

        public SimpleDTO TestSimpleDTO()
        {
            var dto = new SimpleDTO();

            dto.ByteValue = (byte)Constants.DefaultIntReturn;
            dto.SbyteValue = (sbyte)Constants.DefaultIntReturn;
            dto.IntValue = Constants.DefaultIntReturn;
            dto.UintValue = (uint)Constants.DefaultIntReturn;
            dto.ShortValue = (short)Constants.DefaultIntReturn;
            dto.UshortValue = (ushort)Constants.DefaultIntReturn;
            dto.LongValue = (long)Constants.DefaultIntReturn;
            dto.UlongValue = (ulong)Constants.DefaultIntReturn;
            dto.FloatValue = (float)Constants.DefaultIntReturn;
            dto.DoubleValue = (double)Constants.DefaultIntReturn;
            dto.CharValue = Constants.DefaultCharReturn;
            dto.BoolValue = true;
            dto.StringValue = Constants.DefaultStringReturn;

            dto.InnerDTO = new SimpleInnerDTO();
            dto.InnerDTO.IntValue = Constants.DefaultIntReturn;
            dto.InnerDTO.StringValue = Constants.DefaultStringReturn;

            return dto;
        }

        public SimpleNullableDTO TestSimpleNullableDTO(bool withValue)
        {
            var dto = new SimpleNullableDTO();

            dto.InnerDTO = new SimpleInnerNullableDTO();

            if (withValue)
            {
                dto.ByteValue = (byte)Constants.DefaultIntReturn;
                dto.SbyteValue = (sbyte)Constants.DefaultIntReturn;
                dto.IntValue = Constants.DefaultIntReturn;
                dto.UintValue = (uint)Constants.DefaultIntReturn;
                dto.ShortValue = (short)Constants.DefaultIntReturn;
                dto.UshortValue = (ushort)Constants.DefaultIntReturn;
                dto.LongValue = (long)Constants.DefaultIntReturn;
                dto.UlongValue = (ulong)Constants.DefaultIntReturn;
                dto.FloatValue = (float)Constants.DefaultIntReturn;
                dto.DoubleValue = (double)Constants.DefaultIntReturn;
                dto.CharValue = Constants.DefaultCharReturn;
                dto.BoolValue = true;

                dto.InnerDTO.IntValue = Constants.DefaultIntReturn;
                dto.InnerDTO.DoubleValue = (double)Constants.DefaultIntReturn;
            }

            return dto;
        }

        public Task<SimpleDTO> TestSimpleDTOAsync()
        {
            var dto = new SimpleDTO();

            dto.ByteValue = (byte)Constants.DefaultIntReturn;
            dto.SbyteValue = (sbyte)Constants.DefaultIntReturn;
            dto.IntValue = Constants.DefaultIntReturn;
            dto.UintValue = (uint)Constants.DefaultIntReturn;
            dto.ShortValue = (short)Constants.DefaultIntReturn;
            dto.UshortValue = (ushort)Constants.DefaultIntReturn;
            dto.LongValue = (long)Constants.DefaultIntReturn;
            dto.UlongValue = (ulong)Constants.DefaultIntReturn;
            dto.FloatValue = (float)Constants.DefaultIntReturn;
            dto.DoubleValue = (double)Constants.DefaultIntReturn;
            dto.CharValue = Constants.DefaultCharReturn;
            dto.BoolValue = true;
            dto.StringValue = Constants.DefaultStringReturn;

            dto.InnerDTO = new SimpleInnerDTO();
            dto.InnerDTO.IntValue = Constants.DefaultIntReturn;
            dto.InnerDTO.StringValue = Constants.DefaultStringReturn;

            return Task.FromResult(dto);
        }

        public Task<SimpleNullableDTO> TestSimpleNullableDTOAsync(bool withValue)
        {
            var dto = new SimpleNullableDTO();

            dto.InnerDTO = new SimpleInnerNullableDTO();

            if (withValue)
            {
                dto.ByteValue = (byte)Constants.DefaultIntReturn;
                dto.SbyteValue = (sbyte)Constants.DefaultIntReturn;
                dto.IntValue = Constants.DefaultIntReturn;
                dto.UintValue = (uint)Constants.DefaultIntReturn;
                dto.ShortValue = (short)Constants.DefaultIntReturn;
                dto.UshortValue = (ushort)Constants.DefaultIntReturn;
                dto.LongValue = (long)Constants.DefaultIntReturn;
                dto.UlongValue = (ulong)Constants.DefaultIntReturn;
                dto.FloatValue = (float)Constants.DefaultIntReturn;
                dto.DoubleValue = (double)Constants.DefaultIntReturn;
                dto.CharValue = Constants.DefaultCharReturn;
                dto.BoolValue = true;

                dto.InnerDTO.IntValue = Constants.DefaultIntReturn;
                dto.InnerDTO.DoubleValue = (double)Constants.DefaultIntReturn;
            }

            return Task.FromResult(dto);
        }

        public int TestListSum(List<int> numbers)
        {
            return numbers.Sum();
        }
    }
}