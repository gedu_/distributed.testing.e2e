namespace Distributed.Testing.E2E
{
    public static class Constants {
        public static string DefaultUser = "User";
        
        public static string Greet = "Hello, {0}";

        public static int DefaultIntReturn = 13;

        public static char DefaultCharReturn = 'A';

        public static string DefaultStringReturn = "Test";

        public static decimal DefaultDecimalReturn = 10.11M;

        public static string DefaultPubSubMessage = "test";
    }
}