using System.Collections.Generic;
using System.IO;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Extensions.Logging;
using Distributed.Common;
using System;
using Distributed.Testing.E2E;
using System.Linq;
using Microsoft.Extensions.CommandLineUtils;

class Program
{

    public static IServiceLoop ServiceLoop;
    public static IServiceProvider ServerProvider;
    public static List<IServiceProvider> ClientProviders;
    public static Microsoft.Extensions.Logging.ILogger Logger;
    
    static void Main(string[] args)
    {
        BuildObjects();

        var status = 0;

        var failed = false;

        var modes = new Dictionary<string, Func<int>>()
        {
            {"sync", () => SyncTests.Execute(ServiceLoop, ClientProviders, Logger)},
            {"async", () => AsyncTests.Execute(ServiceLoop, ClientProviders, Logger)},
            {"syncpubsub", () => SyncPubSubTests.Execute(ClientProviders, Logger)},
            {"asyncpubsub", () => AsyncPubSubTests.Execute(ClientProviders, Logger)},
            {"syncdto", () => SyncDtoTests.Execute(ServiceLoop, ClientProviders, Logger)},
            {"asyncdto", () => AsyncDtoTests.Execute(ServiceLoop, ClientProviders, Logger)},
        };


        var cmdApp = new CommandLineApplication();

        var modesString = modes.Select(x => x.Key).Aggregate((i, j) => $"{i}, {j}");

        var mode = cmdApp.Option("-m <mode>", $"Test mode. Available: {modesString}", CommandOptionType.SingleValue);

        cmdApp.OnExecute(() => 
        {
           if (mode.HasValue())
           {
                if (!modes.ContainsKey(mode.Value()))
                {
                    Console.WriteLine($"Mode setting is required. Use of one: -m <{modesString}>");

                    failed = true;

                    return 1;
                }

                status += modes[mode.Value()].Invoke();

                return 0;
           }
           
            Console.WriteLine($"Mode setting is required. Use of one: -m <{modesString}>");

            failed = true;

            return 1;

        });

        cmdApp.Execute(args);

        if (failed)
        {
            Environment.Exit(1);
        }

        if (status != 0)
        {
            Logger.LogError("E2E testing failed.");
        }
        else 
        {
            Logger.LogInformation("All tests succesfully passed");
        }

        Environment.Exit(status);
    }

    public static void BuildObjects()
    {
        var collection = new ServiceCollection();
        var workingDir = Directory.GetCurrentDirectory();
        var config = new ConfigurationBuilder()
            .SetBasePath(workingDir)
            .AddJsonFile("settings.json", optional: false, reloadOnChange: true)
            .Build();

        ClientProviders = new List<IServiceProvider>();
        
        Log.Logger = new LoggerConfiguration()
            .Enrich.FromLogContext()
            .WriteTo.RollingFile(Path.Combine(workingDir, config.GetSection("LogFile").Value))
            .WriteTo.ColoredConsole()
            .MinimumLevel.Debug()
            .CreateLogger();

        ServerProvider = collection
            .WithLoggerProvider(new SerilogLoggerProvider(Log.Logger))
            .WithConfig(config)
            .RegisterServiceDependencies(true)
            .BuildServiceProvider();

        //1
        ClientProviders.Add(collection
            .WithLoggerProvider(new SerilogLoggerProvider(Log.Logger))
            .WithConfig(config)
            .RegisterServiceDependencies(false)
            .BuildServiceProvider()
        );

        //2
        ClientProviders.Add(collection
            .WithLoggerProvider(new SerilogLoggerProvider(Log.Logger))
            .WithConfig(config)
            .RegisterServiceDependencies(false)
            .BuildServiceProvider()
        );
        
        //3
        ClientProviders.Add(collection
            .WithLoggerProvider(new SerilogLoggerProvider(Log.Logger))
            .WithConfig(config)
            .RegisterServiceDependencies(false)
            .BuildServiceProvider()
        );

        ServiceLoop = ServerProvider.GetService<IServiceLoop>();

        Logger = ServerProvider.GetService<Microsoft.Extensions.Logging.ILogger>();
    }
}
