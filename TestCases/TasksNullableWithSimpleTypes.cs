using System.Threading.Tasks;
using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class TasksWithSimpleNullableTypes
    {

        public static async Task<int> Test (ITestService testService, ILogger logger)
        {
            var status = 0;

            //results
            var byteResult = (byte?)Constants.DefaultIntReturn;
            var sbyteResult = (sbyte?)Constants.DefaultIntReturn;
            var intResult = (int?)Constants.DefaultIntReturn;
            var uintResult = (uint?)Constants.DefaultIntReturn;
            var shortResult = (short?)Constants.DefaultIntReturn;
            var ushortResult = (ushort?)Constants.DefaultIntReturn;
            var longResult = (long?)Constants.DefaultIntReturn;
            var ulongResult = (ulong?)Constants.DefaultIntReturn;
            var floatResult = (float?)Constants.DefaultIntReturn;
            var doubleResult = (double?)Constants.DefaultIntReturn;
            var charResult = (char?)Constants.DefaultCharReturn;
            var boolResult = (bool?)true;
            var decimalResult = Constants.DefaultDecimalReturn;



            var byteTest = await testService.TestNullableByteAsync(true);

            if (byteTest.Value != byteResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableByteAsync: {byteTest.Value} != {byteResult}");
                status++;
            }

            byteTest = await testService.TestNullableByteAsync(false);

            if (byteTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableByteAsync: expected null got {byteTest.Value}");
                status++;
            }

            /////////////////////////////

            var sbyteTest = await testService.TestNullableSByteAsync(true);

            if (sbyteTest.Value != sbyteResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableSByteAsync: {sbyteTest.Value} != {sbyteResult}");
                status++;
            }

            sbyteTest = await testService.TestNullableSByteAsync(false);

            if (sbyteTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableSByteAsync: expected null got {sbyteTest.Value}");
                status++;
            }

            /////////////////////////////

            var intTest = await testService.TestNullableIntAsync(true);

            if (intTest.Value != intResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableIntAsync: {intTest.Value} != {intResult}");
                status++;
            }

            intTest = await testService.TestNullableIntAsync(false);

            if (intTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableIntAsync: expected null got {intTest.Value}");
                status++;
            }

            /////////////////////////////

            var uintTest = await testService.TestNullableUIntAsync(true);

            if (uintTest.Value != uintResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUIntAsync: {uintTest.Value} != {uintResult}");
                status++;
            }

            uintTest = await testService.TestNullableUIntAsync(false);

            if (uintTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUIntAsync: expected null got {uintTest.Value}");
                status++;
            }

            /////////////////////////////

            var shortTest = await testService.TestNullableShortAsync(true);

            if (shortTest.Value != shortResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableShortAsync: {shortTest.Value} != {shortResult}");
                status++;
            }

            shortTest = await testService.TestNullableShortAsync(false);

            if (shortTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableShortAsync: expected null got {shortTest.Value}");
                status++;
            }

            /////////////////////////////

            var ushortTest = await testService.TestNullableUShortAsync(true);

            if (ushortTest.Value != ushortResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUShortAsync: {ushortTest.Value} != {ushortResult}");
                status++;
            }

            ushortTest = await testService.TestNullableUShortAsync(false);

            if (ushortTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUShortAsync: expected null got {ushortTest.Value}");
                status++;
            }

            /////////////////////////////

            var longTest = await testService.TestNullableLongAsync(true);

            if (longTest.Value != longResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableLongAsync: {longTest.Value} != {longResult}");
                status++;
            }

            longTest = await testService.TestNullableLongAsync(false);

            if (longTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableLongAsync: expected null got {longTest.Value}");
                status++;
            }

            /////////////////////////////

            var ulongTest = await testService.TestNullableULongAsync(true);

            if (ulongTest.Value != ulongResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableULongAsync: {ulongTest.Value} != {ulongResult}");
                status++;
            }

            ulongTest = await testService.TestNullableULongAsync(false);

            if (ulongTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableULongAsync: expected null got {ulongTest.Value}");
                status++;
            }

            /////////////////////////////

            var floatTest = await testService.TestNullableFloatAsync(true);

            if (floatTest.Value != floatResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableFloatAsync: {floatTest.Value} != {floatResult}");
                status++;
            }

            floatTest = await testService.TestNullableFloatAsync(false);

            if (floatTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableFloatAsync: expected null got {floatTest.Value}");
                status++;
            }

            /////////////////////////////

            var doubleTest = await testService.TestNullableDoubleAsync(true);

            if (doubleTest.Value != doubleResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableDoubleAsync: {doubleTest.Value} != {doubleResult}");
                status++;
            }

            doubleTest = await testService.TestNullableDoubleAsync(false);

            if (doubleTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableDoubleAsync: expected null got {doubleTest.Value}");
                status++;
            }

            /////////////////////////////

            var charTest = await testService.TestNullableCharAsync(true);

            if (charTest.Value != charResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableCharAsync: {charTest.Value} != {charResult}");
                status++;
            }

            charTest = await testService.TestNullableCharAsync(false);

            if (charTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableCharAsync: expected null got {charTest.Value}");
                status++;
            }

            /////////////////////////////

            var boolTest = await testService.TestNullableBoolAsync(true);

            if (boolTest.Value != boolResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableBoolAsync: {boolTest.Value} != {boolResult}");
                status++;
            }

            boolTest = await testService.TestNullableBoolAsync(false);

            if (boolTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableBoolAsync: expected null got {boolTest.Value}");
                status++;
            }

            /////////////////////////////

            var decimalTest = await testService.TestNullableDecimalAsync(true);

            if (decimalTest.Value != decimalResult)
            {
                logger.LogCritical($"Wrong return value from method TestSByteAsync: {decimalTest.Value} != {decimalResult}");
                status++;
            }

            decimalTest = await testService.TestNullableDecimalAsync(false);

            if (decimalTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableDecimalAsync: expected null got {decimalTest.Value}");
                status++;
            }

            /////////////////////////////

            return status;
        }
    }
}