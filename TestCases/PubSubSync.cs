using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class PubSubSync
    {
        public static string PubSubTopic = $"E2ETest_{Guid.NewGuid()}";
        public static string PubSubTopicConsuming = $"E2ETest2_{Guid.NewGuid()}";

        public static IPubSubMessenger Pubsub1;

        public static IPubSubMessenger Pubsub2;

        public static IPubSubMessenger Pubsub3;

        public static IPubSubMessenger Sender;

        public static ILogger Logger;

        public static int Test (List<IServiceProvider> clientProviders, ILogger logger)
        {
            var status = 0;

            //Setting up 3 subscribers
            Pubsub1 = (IPubSubMessenger)clientProviders[0].GetService(typeof(IPubSubMessenger));
            Pubsub2 = (IPubSubMessenger)clientProviders[1].GetService(typeof(IPubSubMessenger));
            Pubsub3 = (IPubSubMessenger)clientProviders[2].GetService(typeof(IPubSubMessenger));

            Sender = Pubsub1;

            Logger = logger;

            status += TestNonConsuming();

            status += TestConsuming();

            return status;

        }

        public static int TestNonConsuming()
        {
            var status = 0;
            
            var counter = 0;

            Guid firstSubscription;

            Task.Factory.StartNew(() => firstSubscription =
                Pubsub1.Subscribe(PubSubTopic, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #1");
                    counter = Interlocked.Add(ref counter, 1);
                }, false)
            ).Wait();

            Task.Factory.StartNew(() => Pubsub2.Subscribe(PubSubTopic, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #2");
                    counter = Interlocked.Add(ref counter, 1);
                }, false)
            ).Wait();

            Task.Factory.StartNew(() => Pubsub3.Subscribe(PubSubTopic, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #3");
                    counter = Interlocked.Add(ref counter, 1);
                }, false)
            ).Wait();

            var expectedMessageCount = 3;

            Sender.Publish(PubSubTopic, Constants.DefaultPubSubMessage);

            var timer = new Timer((stateInfo) => {
                if (counter != expectedMessageCount)
                {
                    Logger.LogCritical($"Incorrect number of messages stored after Publish to PubSub. {counter} != {expectedMessageCount}");
                    status++;
                }
            }, null, 2000, 1);

            //////

            Pubsub1.Unsubscribe(firstSubscription);
            Pubsub2.Unsubscribe(firstSubscription);
            Pubsub3.Unsubscribe(firstSubscription);

            //////

            Task.Factory.StartNew(() => 
            {
                Pubsub1.Subscribe(PubSubTopic, (msg) => {
                        Logger.LogDebug("Adding message from subscriber #4 / 1");
                        counter = Interlocked.Add(ref counter, 1);
                    }
                , false);

                Pubsub1.Subscribe(PubSubTopic, (msg) => {
                        Logger.LogDebug("Adding message from subscriber #4 / 2");
                        counter = Interlocked.Add(ref counter, 1);
                    }
                , false);

            }).Wait();

            counter = 0;

            expectedMessageCount = 4;

            Sender.Publish(PubSubTopic, Constants.DefaultPubSubMessage);

            timer = new Timer((stateInfo) => {
                if (counter != expectedMessageCount)
                {
                    Logger.LogCritical($"Incorrect number of messages stored after Publish to PubSub. {counter} != {expectedMessageCount}");
                    status++;
                }
            }, null, 2000, 1);

            return status;
        }

        public static int TestConsuming()
        {
            var status = 0;

            var counter = 0;

            var expectedMessageCount = 1;

            Task.Factory.StartNew(() => Pubsub1.Subscribe(PubSubTopicConsuming, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #1");
                    counter = Interlocked.Add(ref counter, 1);
                }, true)
            ).Wait();

            Task.Factory.StartNew(() => Pubsub2.Subscribe(PubSubTopicConsuming, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #2");
                    counter = Interlocked.Add(ref counter, 1);
                }, true)
            ).Wait();

            Task.Factory.StartNew(() => Pubsub3.Subscribe(PubSubTopicConsuming, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #3");
                    counter = Interlocked.Add(ref counter, 1);
                }, true)
            ).Wait();

            Sender.Publish(PubSubTopicConsuming, Constants.DefaultPubSubMessage);

            var timer = new Timer((stateInfo) => {
                if (counter != expectedMessageCount)
                {
                    Logger.LogCritical($"Incorrect number of messages stored after Publish to PubSub. {counter} != {expectedMessageCount}");
                    status++;
                }
            }, null, 2000, 1);

            return status;
        }
    }

}