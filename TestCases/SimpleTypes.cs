using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class SimpleTypes
    {

        public static int Test (ITestService testService, ILogger logger)
        {
            var status = 0;

            //results
            var byteResult = (byte)Constants.DefaultIntReturn;
            var sbyteResult = (sbyte)Constants.DefaultIntReturn;
            var intResult = Constants.DefaultIntReturn;
            var uintResult = (uint)Constants.DefaultIntReturn;
            var shortResult = (short)Constants.DefaultIntReturn;
            var ushortResult = (ushort)Constants.DefaultIntReturn;
            var longResult = (long)Constants.DefaultIntReturn;
            var ulongResult = (ulong)Constants.DefaultIntReturn;
            var floatResult = (float)Constants.DefaultIntReturn;
            var doubleResult = (double)Constants.DefaultIntReturn;
            var charResult = Constants.DefaultCharReturn;
            var boolResult = true;
            var stringResult = Constants.DefaultStringReturn;
            var decimalResult = Constants.DefaultDecimalReturn;



            var byteTest = testService.TestByte();

            if (byteTest != byteResult)
            {
                logger.LogCritical($"Wrong return value from method TestByte: {byteTest} != {byteResult}");
                status++;
            }

            var sbyteTest = testService.TestSByte();

            if (sbyteTest != sbyteResult)
            {
                logger.LogCritical($"Wrong return value from method TestSByte: {sbyteTest} != {sbyteResult}");
                status++;
            }

            var intTest = testService.TestInt();

            if (intTest != intResult)
            {
                logger.LogCritical($"Wrong return value from method TestInt: {intTest} != {intResult}");
                status++;
            }

            var uintTset = testService.TestUInt();

            if (uintTset != uintResult)
            {
                logger.LogCritical($"Wrong return value from method TestUInt: {uintTset} != {uintResult}");
                status++;
            }

            var shortTest = testService.TestShort();

            if (shortTest != shortResult)
            {
                logger.LogCritical($"Wrong return value from method TestShort: {shortTest} != {shortResult}");
                status++;
            }

            var ushortTest = testService.TestUShort();

            if (ushortTest != ushortResult)
            {
                logger.LogCritical($"Wrong return value from method TestUShort: {ushortTest} != {ushortResult}");
                status++;
            }

            var longTest = testService.TestLong();

            if (longTest != longResult)
            {
                logger.LogCritical($"Wrong return value from method TestLong: {longTest} != {longResult}");
                status++;
            }

            var ulongTest = testService.TestULong();

            if (ulongTest != ulongResult)
            {
                logger.LogCritical($"Wrong return value from method TestULong: {ulongTest} != {ulongResult}");
                status++;
            }

            var floatTest = testService.TestFloat();

            if (floatTest != floatResult)
            {
                logger.LogCritical($"Wrong return value from method TestFloat: {floatTest} != {floatResult}");
                status++;
            }

            var doubleTest = testService.TestDouble();

            if (doubleTest != doubleResult)
            {
                logger.LogCritical($"Wrong return value from method TestDouble: {doubleTest} != {doubleResult}");
                status++;
            }

            var charTest = testService.TestChar();

            if (charTest != charResult)
            {
                logger.LogCritical($"Wrong return value from method TestChar: {charTest} != {charResult}");
                status++;
            }

            var boolTest = testService.TestBool();

            if (boolTest != boolResult)
            {
                logger.LogCritical($"Wrong return value from method TestBool: {boolTest} != {boolResult}");
                status++;
            }

            var stringTest = testService.TestString();

            if (stringTest != stringResult)
            {
                logger.LogCritical($"Wrong return value from method TestString: {stringTest} != {stringResult}");
                status++;
            }

            var decimalTest = testService.TestDecimal();

            if (decimalTest != decimalResult)
            {
                logger.LogCritical($"Wrong return value from method TestDecimal: {decimalTest} != {decimalResult}");
                status++;
            }


            return status;
        }
    }
}