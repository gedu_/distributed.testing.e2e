using System.Threading.Tasks;
using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class TasksWithSimpleTypes
    {

        public static async Task<int> Test (ITestService testService, ILogger logger)
        {

            var status = 0;

            //results
            var byteResult = (byte)Constants.DefaultIntReturn;
            var sbyteResult = (sbyte)Constants.DefaultIntReturn;
            var intResult = Constants.DefaultIntReturn;
            var uintResult = (uint)Constants.DefaultIntReturn;
            var shortResult = (short)Constants.DefaultIntReturn;
            var ushortResult = (ushort)Constants.DefaultIntReturn;
            var longResult = (long)Constants.DefaultIntReturn;
            var ulongResult = (ulong)Constants.DefaultIntReturn;
            var floatResult = (float)Constants.DefaultIntReturn;
            var doubleResult = (double)Constants.DefaultIntReturn;
            var charResult = Constants.DefaultCharReturn;
            var boolResult = true;
            var stringResult = Constants.DefaultStringReturn;
            var decimalResult = Constants.DefaultDecimalReturn;



            var byteTest = await testService.TestByteAsync();

            if (byteTest != byteResult)
            {
                logger.LogCritical($"Wrong return value from method TestByteAsync: {byteTest} != {byteResult}");
                status++;
            }

            var sbyteTest = await testService.TestSByteAsync();

            if (sbyteTest != sbyteResult)
            {
                logger.LogCritical($"Wrong return value from method TestSByteAsync: {sbyteTest} != {sbyteResult}");
                status++;
            }

            var intTest = await testService.TestIntAsync();

            if (intTest != intResult)
            {
                logger.LogCritical($"Wrong return value from method TestIntAsync: {intTest} != {intResult}");
                status++;
            }

            var uintTset = await testService.TestUIntAsync();

            if (uintTset != uintResult)
            {
                logger.LogCritical($"Wrong return value from method TestUIntAsync: {uintTset} != {uintResult}");
                status++;
            }

            var shortTest = await testService.TestShortAsync();

            if (shortTest != shortResult)
            {
                logger.LogCritical($"Wrong return value from method TestShortAsync: {shortTest} != {shortResult}");
                status++;
            }

            var ushortTest = await testService.TestUShortAsync();

            if (ushortTest != ushortResult)
            {
                logger.LogCritical($"Wrong return value from method TestUShortAsync: {ushortTest} != {ushortResult}");
                status++;
            }

            var longTest = await testService.TestLongAsync();

            if (longTest != longResult)
            {
                logger.LogCritical($"Wrong return value from method TestLongAsync: {longTest} != {longResult}");
                status++;
            }

            var ulongTest = await testService.TestULongAsync();

            if (ulongTest != ulongResult)
            {
                logger.LogCritical($"Wrong return value from method TestULongAsync: {ulongTest} != {ulongResult}");
                status++;
            }

            var floatTest = await testService.TestFloatAsync();

            if (floatTest != floatResult)
            {
                logger.LogCritical($"Wrong return value from method TestFloatAsync: {floatTest} != {floatResult}");
                status++;
            }

            var doubleTest = await testService.TestDoubleAsync();

            if (doubleTest != doubleResult)
            {
                logger.LogCritical($"Wrong return value from method TestDoubleAsync: {doubleTest} != {doubleResult}");
                status++;
            }

            var charTest = await testService.TestCharAsync();

            if (charTest != charResult)
            {
                logger.LogCritical($"Wrong return value from method TestCharAsync: {charTest} != {charResult}");
                status++;
            }

            var boolTest = await testService.TestBoolAsync();

            if (boolTest != boolResult)
            {
                logger.LogCritical($"Wrong return value from method TestBoolAsync: {boolTest} != {boolResult}");
                status++;
            }

            var stringTest = await testService.TestStringAsync();

            if (stringTest != stringResult)
            {
                logger.LogCritical($"Wrong return value from method TestStringAsync: {stringTest} != {stringResult}");
                status++;
            }

            var decimalTest = await testService.TestDecimalAsync();

            if (decimalTest != decimalResult)
            {
                logger.LogCritical($"Wrong return value from method TestDecimalAsync: {decimalTest} != {decimalResult}");
                status++;
            }

            return status;
        }
    }
}