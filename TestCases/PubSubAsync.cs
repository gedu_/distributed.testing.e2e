using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Distributed.Common.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class PubSubAsync
    {
        public static string PubSubTopic = $"E2ETest3_{Guid.NewGuid()}";
        public static string PubSubTopicConsuming = $"E2ETest4_{Guid.NewGuid()}";

        public static IPubSubMessenger Pubsub1;

        public static IPubSubMessenger Pubsub2;

        public static IPubSubMessenger Pubsub3;

        public static IPubSubMessenger Sender;

        public static ILogger Logger;

        public static int Test (List<IServiceProvider> clientProviders, ILogger logger)
        {
            var status = 0;

            //Setting up 3 subscribers
            Pubsub1 = (IPubSubMessenger)clientProviders[0].GetService(typeof(IPubSubMessenger));
            Pubsub2 = (IPubSubMessenger)clientProviders[1].GetService(typeof(IPubSubMessenger));
            Pubsub3 = (IPubSubMessenger)clientProviders[2].GetService(typeof(IPubSubMessenger));

            Sender = Pubsub1;

            Logger = logger;

            status += TestNonConsuming().Result;

            status += TestConsuming().Result;

            return status;

        }

        public static async Task<int> TestNonConsuming()
        {
            var status = 0;
            
            var counter = 0;

            Guid firstSubscription;

            await Task.Factory.StartNew(async () => firstSubscription =
                await Pubsub1.SubscribeAsync(PubSubTopic, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #1");
                    counter = Interlocked.Add(ref counter, 1);
                }, false)
            );

            await Task.Factory.StartNew(async () => await Pubsub2.SubscribeAsync(PubSubTopic, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #2");
                    counter = Interlocked.Add(ref counter, 1);
                }, false)
            );

            await Task.Factory.StartNew(async () => await Pubsub3.SubscribeAsync(PubSubTopic, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #3");
                    counter = Interlocked.Add(ref counter, 1);
                }, false)
            );

            var expectedMessageCount = 3;

            await Sender.PublishAsync(PubSubTopic, Constants.DefaultPubSubMessage);

            var timer = new Timer((stateInfo) => {
                if (counter != expectedMessageCount)
                {
                    Logger.LogCritical($"Incorrect number of messages stored after Publish to PubSub. {counter} != {expectedMessageCount}");
                    status++;
                }
            }, null, 2000, 1); 

            //////

            await Pubsub1.UnsubscribeAsync(firstSubscription);
            await Pubsub2.UnsubscribeAsync(firstSubscription);
            await Pubsub3.UnsubscribeAsync(firstSubscription);

            //////

            await Task.Factory.StartNew(async () => 
            {
                await Pubsub1.SubscribeAsync(PubSubTopic, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #4 / 1");
                    counter = Interlocked.Add(ref counter, 1);
                    }
                , false);

                await Pubsub1.SubscribeAsync(PubSubTopic, (msg) => {
                        Logger.LogDebug("Adding message from subscriber #4 / 2");
                        counter = Interlocked.Add(ref counter, 1);
                    }
                , false);

            });

            counter = 0;

            expectedMessageCount = 4;

            await Sender.PublishAsync(PubSubTopic, Constants.DefaultPubSubMessage);

            timer = new Timer((stateInfo) => {
                if (counter != expectedMessageCount)
                {
                    Logger.LogCritical($"Incorrect number of messages stored after Publish to PubSub. {counter} != {expectedMessageCount}");
                    status++;
                }
            }, null, 2000, 1);  

            return status;
        }

        public static async Task<int> TestConsuming()
        {
            var status = 0;

            var counter = 0;

            var expectedMessageCount = 1;

            await Task.Factory.StartNew(async () => await Pubsub1.SubscribeAsync(PubSubTopicConsuming, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #1");
                    counter = Interlocked.Add(ref counter, 1);
                }, true)
            );

            await Task.Factory.StartNew(async () => await Pubsub2.SubscribeAsync(PubSubTopicConsuming, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #2");
                    counter = Interlocked.Add(ref counter, 1);
                }, true)
            );

            await Task.Factory.StartNew(async () => await Pubsub3.SubscribeAsync(PubSubTopicConsuming, (msg) => {
                    Logger.LogDebug("Adding message from subscriber #3");
                    counter = Interlocked.Add(ref counter, 1);
                }, true)
            );

            await Sender.PublishAsync(PubSubTopicConsuming, Constants.DefaultPubSubMessage);

            var timer = new Timer((stateInfo) => {
                if (counter != expectedMessageCount)
                {
                    Logger.LogCritical($"Incorrect number of messages stored after Publish to PubSub. {counter} != {expectedMessageCount}");
                    status++;
                }
            }, null, 2000, 1);         

            return status;
        }
    }

}