using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class SimpleNullableTypes
    {

        public static int Test (ITestService testService, ILogger logger)
        {
            var status = 0;

            //results
            var byteResult = (byte?)Constants.DefaultIntReturn;
            var sbyteResult = (sbyte?)Constants.DefaultIntReturn;
            var intResult = (int?)Constants.DefaultIntReturn;
            var uintResult = (uint?)Constants.DefaultIntReturn;
            var shortResult = (short?)Constants.DefaultIntReturn;
            var ushortResult = (ushort?)Constants.DefaultIntReturn;
            var longResult = (long?)Constants.DefaultIntReturn;
            var ulongResult = (ulong?)Constants.DefaultIntReturn;
            var floatResult = (float?)Constants.DefaultIntReturn;
            var doubleResult = (double?)Constants.DefaultIntReturn;
            var charResult = (char?)Constants.DefaultCharReturn;
            var boolResult = (bool?)true;
            var decimalResult = Constants.DefaultDecimalReturn;



            var byteTest = testService.TestNullableByte(true);

            if (byteTest.Value != byteResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableByte: {byteTest.Value} != {byteResult}");
                status++;
            }

            byteTest = testService.TestNullableByte(false);

            if (byteTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableByte: expected null got {byteTest.Value}");
                status++;
            }

            /////////////////////////////

            var sbyteTest = testService.TestNullableSByte(true);

            if (sbyteTest.Value != sbyteResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableSByte: {sbyteTest.Value} != {sbyteResult}");
                status++;
            }

            sbyteTest = testService.TestNullableSByte(false);

            if (sbyteTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableSByte: expected null got {sbyteTest.Value}");
                status++;
            }

            /////////////////////////////

            var intTest = testService.TestNullableInt(true);

            if (intTest.Value != intResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableInt: {intTest.Value} != {intResult}");
                status++;
            }

            intTest = testService.TestNullableInt(false);

            if (intTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableInt: expected null got {intTest.Value}");
                status++;
            }

            /////////////////////////////

            var uintTest = testService.TestNullableUInt(true);

            if (uintTest.Value != uintResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUInt: {uintTest.Value} != {uintResult}");
                status++;
            }

            uintTest = testService.TestNullableUInt(false);

            if (uintTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUInt: expected null got {uintTest.Value}");
                status++;
            }

            /////////////////////////////

            var shortTest = testService.TestNullableShort(true);

            if (shortTest.Value != shortResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableShort: {shortTest.Value} != {shortResult}");
                status++;
            }

            shortTest = testService.TestNullableShort(false);

            if (shortTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableShort: expected null got {shortTest.Value}");
                status++;
            }

            /////////////////////////////

            var ushortTest = testService.TestNullableUShort(true);

            if (ushortTest.Value != ushortResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUShort: {ushortTest.Value} != {ushortResult}");
                status++;
            }

            ushortTest = testService.TestNullableUShort(false);

            if (ushortTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableUShort: expected null got {ushortTest.Value}");
                status++;
            }

            /////////////////////////////

            var longTest = testService.TestNullableLong(true);

            if (longTest.Value != longResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableLong: {longTest.Value} != {longResult}");
                status++;
            }

            longTest = testService.TestNullableLong(false);

            if (longTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableLong: expected null got {longTest.Value}");
                status++;
            }

            /////////////////////////////

            var ulongTest = testService.TestNullableULong(true);

            if (ulongTest.Value != ulongResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableULong: {ulongTest.Value} != {ulongResult}");
                status++;
            }

            ulongTest = testService.TestNullableULong(false);

            if (ulongTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableULong: expected null got {ulongTest.Value}");
                status++;
            }

            /////////////////////////////

            var floatTest = testService.TestNullableFloat(true);

            if (floatTest.Value != floatResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableFloat: {floatTest.Value} != {floatResult}");
                status++;
            }

            floatTest = testService.TestNullableFloat(false);

            if (floatTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableFloat: expected null got {floatTest.Value}");
                status++;
            }

            /////////////////////////////

            var doubleTest = testService.TestNullableDouble(true);

            if (doubleTest.Value != doubleResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableDouble: {doubleTest.Value} != {doubleResult}");
                status++;
            }

            doubleTest = testService.TestNullableDouble(false);

            if (doubleTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableDouble: expected null got {doubleTest.Value}");
                status++;
            }

            /////////////////////////////

            var charTest = testService.TestNullableChar(true);

            if (charTest.Value != charResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableChar: {charTest.Value} != {charResult}");
                status++;
            }

            charTest = testService.TestNullableChar(false);

            if (charTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableChar: expected null got {charTest.Value}");
                status++;
            }

            /////////////////////////////

            var boolTest = testService.TestNullableBool(true);

            if (boolTest.Value != boolResult)
            {
                logger.LogCritical($"Wrong return value from method TestNullableBool: {boolTest.Value} != {boolResult}");
                status++;
            }

            boolTest = testService.TestNullableBool(false);

            if (boolTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableBool: expected null got {boolTest.Value}");
                status++;
            }

            /////////////////////////////

            var decimalTest = testService.TestNullableDecimal(true);

            if (decimalTest.Value != decimalResult)
            {
                logger.LogCritical($"Wrong return value from method TestSByte: {decimalTest.Value} != {decimalResult}");
                status++;
            }

            decimalTest = testService.TestNullableDecimal(false);

            if (decimalTest.HasValue)
            {
                logger.LogCritical($"Wrong return value from method TestNullableDecimal: expected null got {decimalTest.Value}");
                status++;
            }

            /////////////////////////////

            return status;
        }
    }
}