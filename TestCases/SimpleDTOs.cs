using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class SimpleDTOs
    {

        public static int Test (ITestService testService, ILogger logger)
        {
            var status = 0;

            //results
            var byteResult = (byte)Constants.DefaultIntReturn;
            var sbyteResult = (sbyte)Constants.DefaultIntReturn;
            var intResult = Constants.DefaultIntReturn;
            var uintResult = (uint)Constants.DefaultIntReturn;
            var shortResult = (short)Constants.DefaultIntReturn;
            var ushortResult = (ushort)Constants.DefaultIntReturn;
            var longResult = (long)Constants.DefaultIntReturn;
            var ulongResult = (ulong)Constants.DefaultIntReturn;
            var floatResult = (float)Constants.DefaultIntReturn;
            var doubleResult = (double)Constants.DefaultIntReturn;
            var charResult = Constants.DefaultCharReturn;
            var boolResult = true;
            var stringResult = Constants.DefaultStringReturn;

            var result = testService.TestSimpleDTO();

            if (result.ByteValue != byteResult)
            {
                logger.LogCritical($"Wrong byte value in SimpleDTO: {result.ByteValue} != {byteResult}");
                status++;
            }

            if (result.SbyteValue != sbyteResult)
            {
                logger.LogCritical($"Wrong sbyte value in SimpleDTO: {result.SbyteValue} != {sbyteResult}");
                status++;
            }

            if (result.IntValue != intResult)
            {
                logger.LogCritical($"Wrong int value in SimpleDTO: {result.IntValue} != {intResult}");
                status++;
            }

            if (result.UintValue != uintResult)
            {
                logger.LogCritical($"Wrong uint value in SimpleDTO: {result.UintValue} != {uintResult}");
                status++;
            }

            if (result.ShortValue != shortResult)
            {
                logger.LogCritical($"Wrong short value in SimpleDTO: {result.ShortValue} != {shortResult}");
                status++;
            }

            if (result.UshortValue != ushortResult)
            {
                logger.LogCritical($"Wrong ushort value in SimpleDTO: {result.UshortValue} != {ushortResult}");
                status++;
            }

            if (result.LongValue != longResult)
            {
                logger.LogCritical($"Wrong long value in SimpleDTO: {result.LongValue} != {longResult}");
                status++;
            }

            if (result.UlongValue != ulongResult)
            {
                logger.LogCritical($"Wrong ulong value in SimpleDTO: {result.UlongValue} != {ulongResult}");
                status++;
            }

            if (result.FloatValue != floatResult)
            {
                logger.LogCritical($"Wrong float value in SimpleDTO: {result.FloatValue} != {floatResult}");
                status++;
            }

            if (result.DoubleValue != doubleResult)
            {
                logger.LogCritical($"Wrong double value in SimpleDTO: {result.DoubleValue} != {doubleResult}");
                status++;
            }

            if (result.CharValue != charResult)
            {
                logger.LogCritical($"Wrong char value in SimpleDTO: {result.CharValue} != {charResult}");
                status++;
            }

            if (result.BoolValue != boolResult)
            {
                logger.LogCritical($"Wrong bool value in SimpleDTO: {result.BoolValue} != {boolResult}");
                status++;
            }

            if (result.StringValue != stringResult)
            {
                logger.LogCritical($"Wrong string value in SimpleDTO: {result.StringValue} != {stringResult}");
                status++;
            }

            if (result.InnerDTO.IntValue != intResult)
            {
                logger.LogCritical($"Wrong int value in SimpleDTO InnerDTO: {result.InnerDTO.IntValue} != {intResult}");
                status++;
            }

            if (result.InnerDTO.StringValue != stringResult)
            {
                logger.LogCritical($"Wrong string value in SimpleDTO InnerDTO: {result.InnerDTO.StringValue} != {stringResult}");
                status++;
            }


            return status;
        }
    }
}