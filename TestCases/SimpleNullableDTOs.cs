using Distributed.Testing.E2E.Interfaces;
using Microsoft.Extensions.Logging;

namespace Distributed.Testing.E2E
{
    public static class SimpleNullableDTOs
    {

        public static int Test (ITestService testService, ILogger logger)
        {
            var status = 0;

            //results
            var byteResult = (byte)Constants.DefaultIntReturn;
            var sbyteResult = (sbyte)Constants.DefaultIntReturn;
            var intResult = Constants.DefaultIntReturn;
            var uintResult = (uint)Constants.DefaultIntReturn;
            var shortResult = (short)Constants.DefaultIntReturn;
            var ushortResult = (ushort)Constants.DefaultIntReturn;
            var longResult = (long)Constants.DefaultIntReturn;
            var ulongResult = (ulong)Constants.DefaultIntReturn;
            var floatResult = (float)Constants.DefaultIntReturn;
            var doubleResult = (double)Constants.DefaultIntReturn;
            var charResult = Constants.DefaultCharReturn;
            var boolResult = true;
            var stringResult = Constants.DefaultStringReturn;

            var result = testService.TestSimpleNullableDTO(true);

            if (result.ByteValue != byteResult)
            {
                logger.LogCritical($"Wrong byte value in SimpleNullableDTO: {result.ByteValue} != {byteResult}");
                status++;
            }

            if (result.SbyteValue != sbyteResult)
            {
                logger.LogCritical($"Wrong sbyte value in SimpleNullableDTO: {result.SbyteValue} != {sbyteResult}");
                status++;
            }

            if (result.IntValue != intResult)
            {
                logger.LogCritical($"Wrong int value in SimpleNullableDTO: {result.IntValue} != {intResult}");
                status++;
            }

            if (result.UintValue != uintResult)
            {
                logger.LogCritical($"Wrong uint value in SimpleNullableDTO: {result.UintValue} != {uintResult}");
                status++;
            }

            if (result.ShortValue != shortResult)
            {
                logger.LogCritical($"Wrong short value in SimpleNullableDTO: {result.ShortValue} != {shortResult}");
                status++;
            }

            if (result.UshortValue != ushortResult)
            {
                logger.LogCritical($"Wrong ushort value in SimpleNullableDTO: {result.UshortValue} != {ushortResult}");
                status++;
            }

            if (result.LongValue != longResult)
            {
                logger.LogCritical($"Wrong long value in SimpleNullableDTO: {result.LongValue} != {longResult}");
                status++;
            }

            if (result.UlongValue != ulongResult)
            {
                logger.LogCritical($"Wrong ulong value in SimpleNullableDTO: {result.UlongValue} != {ulongResult}");
                status++;
            }

            if (result.FloatValue != floatResult)
            {
                logger.LogCritical($"Wrong float value in SimpleNullableDTO: {result.FloatValue} != {floatResult}");
                status++;
            }

            if (result.DoubleValue != doubleResult)
            {
                logger.LogCritical($"Wrong double value in SimpleNullableDTO: {result.DoubleValue} != {doubleResult}");
                status++;
            }

            if (result.CharValue != charResult)
            {
                logger.LogCritical($"Wrong char value in SimpleNullableDTO: {result.CharValue} != {charResult}");
                status++;
            }

            if (result.BoolValue != boolResult)
            {
                logger.LogCritical($"Wrong bool value in SimpleNullableDTO: {result.BoolValue} != {boolResult}");
                status++;
            }

            if (result.InnerDTO.IntValue != intResult)
            {
                logger.LogCritical($"Wrong int value in SimpleNullableDTO InnerNullableDTO: {result.InnerDTO.IntValue} != {intResult}");
                status++;
            }

            if (result.InnerDTO.DoubleValue != doubleResult)
            {
                logger.LogCritical($"Wrong double value in SimpleNullableDTO InnerNullableDTO: {result.InnerDTO.DoubleValue} != {doubleResult}");
                status++;
            }

            ////////////

            result = testService.TestSimpleNullableDTO(false);

            if (result.ByteValue.HasValue)
            {
                logger.LogCritical($"Wrong byte value in SimpleNullableDTO: expected null got {result.ByteValue.Value}");
                status++;
            }

            if (result.SbyteValue.HasValue)
            {
                logger.LogCritical($"Wrong sbyte value in SimpleNullableDTO: expected null got {result.SbyteValue.Value}");
                status++;
            }

            if (result.IntValue.HasValue)
            {
                logger.LogCritical($"Wrong int value in SimpleNullableDTO: expected null got {result.IntValue.Value}");
                status++;
            }

            if (result.UintValue.HasValue)
            {
                logger.LogCritical($"Wrong uint value in SimpleNullableDTO: expected null got {result.UintValue.Value}");
                status++;
            }

            if (result.ShortValue.HasValue)
            {
                logger.LogCritical($"Wrong short value in SimpleNullableDTO: expected null got {result.ShortValue.Value}");
                status++;
            }

            if (result.UshortValue.HasValue)
            {
                logger.LogCritical($"Wrong ushort value in SimpleNullableDTO: expected null got {result.UshortValue.Value}");
                status++;
            }

            if (result.LongValue.HasValue)
            {
                logger.LogCritical($"Wrong long value in SimpleNullableDTO: expected null got {result.LongValue.Value}");
                status++;
            }

            if (result.UlongValue.HasValue)
            {
                logger.LogCritical($"Wrong ulong value in SimpleNullableDTO: expected null got {result.UlongValue.Value}");
                status++;
            }

            if (result.FloatValue.HasValue)
            {
                logger.LogCritical($"Wrong float value in SimpleNullableDTO: expected null got {result.FloatValue.Value}");
                status++;
            }

            if (result.DoubleValue.HasValue)
            {
                logger.LogCritical($"Wrong double value in SimpleNullableDTO: expected null got {result.DoubleValue.Value}");
                status++;
            }

            if (result.CharValue.HasValue)
            {
                logger.LogCritical($"Wrong char value in SimpleNullableDTO: expected null got {result.CharValue.Value}");
                status++;
            }

            if (result.BoolValue.HasValue)
            {
                logger.LogCritical($"Wrong bool value in SimpleNullableDTO: expected null got {result.BoolValue.Value}");
                status++;
            }

            if (result.InnerDTO.IntValue.HasValue)
            {
                logger.LogCritical($"Wrong int value in SimpleNullableDTO InnerNullableDTO: expected null got {result.InnerDTO.IntValue.Value}");
                status++;
            }

            if (result.InnerDTO.DoubleValue.HasValue)
            {
                logger.LogCritical($"Wrong double value in SimpleNullableDTO InnerNullableDTO: expected null got {result.InnerDTO.DoubleValue.Value}");
                status++;
            }

            return status;
        }
    }
}